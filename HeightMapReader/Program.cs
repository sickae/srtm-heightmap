﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Text;
using System.Linq;

namespace HeightMapReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //HeightMap hm = HeightMap.Parse(@"E:\Development\RDI\Projects\HeightMapReader\Height files\N14\N54W100", 0);
            //hm.Save("N54W100_new");
            while (true)
            {
                try
                {
                    Console.Write("HGT file path: ");
                    string path = Console.ReadLine();
                    Console.Write("Sea level (-32767 to 32767): ");
                    short sealevel = Convert.ToInt16(Console.ReadLine());
                    Console.WriteLine("Processing...");
                    HeightMap hm = HeightMap.Parse(@path, sealevel);
                    Console.Clear();
                    Console.WriteLine("Processing done.");
                    Console.Write("Output filename: ");
                    string filename = Console.ReadLine();
                    Console.WriteLine("Saving...");
                    hm.Save(filename);
                    Console.Clear();
                    Console.WriteLine("File saved.");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    if (ex is FormatException || ex is OverflowException)
                    {
                        Console.WriteLine("Invalid format!");
                        Console.ReadKey();
                        continue;
                    }
                    else if (ex is FileNotFoundException)
                    {
                        Console.WriteLine("File not found!");
                        Console.ReadKey();
                        continue;
                    }
                    else Console.WriteLine(ex.StackTrace);
                }
                break;
            }
        }
    }

    class HeightMap
    {
        short[] value { get; }
        short sealevel { get; }
        short[] extremes;

        public HeightMap(short[] values, short sealevel)
        {
            this.sealevel = sealevel;
            value = new short[values.Length];
            Array.Copy(values, value, values.Length);
            extremes = GetExtremes();
        }

        public short[] Value
        {
            get { return value; }
        }

        public short SeaLevel
        {
            get { return sealevel; }
        }

        short[] GetExtremes() => new short[] { value.Min(x => { return x == short.MinValue ? short.MaxValue : x; }), value.Max() };

        //public static HeightMap Parse(string file_path, short sealevel)
        //{
        //    try
        //    {
        //        StringBuilder ext = new StringBuilder();
        //        for (int i = 1; i <= 4; i++) ext.Insert(0, file_path[file_path.Length - i]);
        //        BinaryReader br = new BinaryReader(File.Open(@file_path + (ext.ToString().Equals(".hgt") ? "" : ".hgt"), FileMode.Open));
        //        List<short> elevations = new List<short>();
        //        while (br.BaseStream.Position != br.BaseStream.Length) elevations.Add((short)(br.ReadByte() + (br.ReadByte() << 8)));
        //        br.Close();
        //        return new HeightMap(elevations.ToArray(), sealevel);
        //    }
        //    catch (FileNotFoundException)
        //    {
        //        throw;
        //    }
        //}

        public static HeightMap Parse(string file_path, short sealevel)
        {
            try
            {
                StringBuilder ext = new StringBuilder();
                for (int i = 1; i <= 4; i++) ext.Insert(0, file_path[file_path.Length - i]);
                byte[] buffer = File.ReadAllBytes(@file_path + (ext.ToString().Equals(".hgt") ? "" : ".hgt"));
                List<short> elevations = new List<short>();
                for (int i = 0; i < buffer.Length; i++) elevations.Add((short)(buffer[i] << 8 + buffer[++i]));
                return new HeightMap(elevations.ToArray(), sealevel);
            }
            catch (FileNotFoundException)
            {
                throw;
            }
        }

        public void Save(string filename)
        {
            StringBuilder ext = new StringBuilder();
            for (int i = 1; i <= 4; i++) ext.Insert(0, filename[filename.Length - i]);
            Bitmap bmp = new Bitmap((int)Math.Sqrt(value.Length), (int)Math.Sqrt(value.Length));
            double modifier = 200.0 / 255.0;
            for (int i = 0; i < value.Length; i++)
            {
                Color c;
                if (value[i] == short.MinValue) c = Color.FromArgb(255, 0, 0);
                else if (value[i] > sealevel) c = Color.FromArgb(0, (int)Math.Abs(value[i] / 256 * modifier) + 55, 0);
                else c = Color.FromArgb(0, 0, (int)Math.Abs(value[i] / 256 * modifier) + 55);
                bmp.SetPixel(i % bmp.Width, i / bmp.Width, c);
            }
            bmp.Save(filename + (ext.ToString().Equals(".bmp") ? "" : ".bmp"));
        }

        //public void Save(string dir_path, string filename)
        //{
        //    Bitmap bmp = new Bitmap((int)Math.Sqrt(value.Length), (int)Math.Sqrt(value.Length));
        //    for (int i = 0; i < value.Length; i++)
        //    {
        //        Color c;
        //        if (value[i] > sealevel) c = Color.FromArgb(0, Math.Abs(value[i] % 255), 0);
        //        else c = Color.FromArgb(0, 0, Math.Abs(value[i] % 255));
        //        bmp.SetPixel(i / bmp.Width, i % bmp.Width, c);
        //    }
        //    bmp.Save(@dir_path + (dir_path[dir_path.Length-1] == '\\' ? "" : "\\") + filename + ".bmp");
        //}
    }
}
